﻿
var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var S3Adapter = require('parse-server').S3Adapter;
var bodyParser = require('body-parser');

var omise = require('omise')({
    'publicKey': 'pkey_test_52uwdobi5bmzhp03pxr',
    'secretKey': 'skey_test_52uwdobi56579vzqyhl'
});


var api = new ParseServer({
    databaseURI: 'mongodb://dbadmin:admin@ds059215.mongolab.com:59215/yourtshirtdesign_alpha',
    //cloud: './cloud/main.js',
    appId: 'WhAmgtyQMIGrpkP6ITS0J7Wh72HHUAFMTESWzEdf',
    fileKey: '12ccad9b-8bde-458f-ac14-c8289657f19f',
    masterKey: '1AZBscw0d0tYQk26QUZycroZKKsPv6gg2FML2bYK',
    facebookAppIds: '164265673961160',
    oauth: {
        facebook: {
            appIds: "164265673961160"
        }
    },
    filesAdapter: new S3Adapter(
        "AKIAJTQQYOW2OKVLHWPA",
        "cG3bJjVVlpreG2mEMNbb4/5+7ODN9PU+ofUaJm9D",       
        {
            bucket: "designdata-shirtdesigndata-imgfile",
            directAccess: true,
            region: 'ap-southeast-1'
        }),

    //S3Adapter constructor options
    //new S3Adapter(accessKey, secretKey, bucket, options)

    //clientKey: 'lHHmSVTM4mDwPrkUdwNkGRsdH0lem8OMc2AGHI1u',
    //restAPIKey: 'jgDUgvH8nCN5ci3mH9grLU4P89JRWBGSAyJY30Nq',
    //javascriptKey: 'fIj6ZlJWPLwVyU9mddIwFNiZUYQEifk3InbpAi6i',
    //dotNetKey: 'BmodBVWtk2bBWWZJ6vxhrqwhke9C3bBtu55oKaTo',
});

var app = express();


app.use('/parse', api);

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

var paymentRouter = express.Router();

paymentRouter.get('/placeorder/:omiseID/:cardID/:amount/:orderID', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;
    var cardID = req.params.cardID;
    var amount = req.params.amount;
    var orderID = req.params.orderID;
    amount = amount + '00'; //add decimal digits for omise.


    omise.charges.create({
        'description': 'Charge for order ID: ' + orderID,
        'amount': amount, // 1,000 Baht
        'currency': 'thb',
        'customer': omiseID,
        'capture': true, //this one is buggy, to set it to 'false' just remove it.
        'card': cardID
    }, function (err, response) {
        if (response.paid) {
            //Success
            console.log('success');
            res.json(response);
        } else {
            //Handle failure
            throw response.failure_code;
        }
    });

})

paymentRouter.get('/create/:email', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");


    var email = req.params.email;

    omise.customers.create({
        email: email
    }, function (error, customer) {
        /* Response. */

        res.json(customer);
    });
})

paymentRouter.get('/retrievecards/:omiseID', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;

    omise.customers.listCards(omiseID, function (error, cardList) {
        /* Response. */
        res.json(cardList);
    });
})

paymentRouter.get('/attachcard/:omiseID/:token', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;
    var token = req.params.token;

    omise.customers.update(omiseID, { 'card': token }, function (error, customer) {
        /* Response. */
        res.json(customer);
    }
);


})

paymentRouter.get('/deletecard/:omiseID/:cardID', function (req, res) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var omiseID = req.params.omiseID;
    var cardID = req.params.cardID;

    omise.customers.destroyCard(
      omiseID,
      cardID,
      function (error, card) {
          /* Response. */
          res.json(card);
      }
    );

})

app.use('/payment', paymentRouter);


var port = process.env.PORT || 1337;

app.listen(port, function () {
    //console.log('parse-server-example running on port ' + port + '.');
});

