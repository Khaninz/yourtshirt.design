﻿

Parse.initialize("WhAmgtyQMIGrpkP6ITS0J7Wh72HHUAFMTESWzEdf", "fIj6ZlJWPLwVyU9mddIwFNiZUYQEifk3InbpAi6i");
//Parse.serverURL = 'http://localhost:1337/parse' //Diriect the sdk to connect to local server (for now), this will match .lister on server.js
Parse.serverURL = 'http://parseserver-updj2-env.ap-southeast-1.elasticbeanstalk.com/parse';
angular.module('admin', []);

angular.module('admin')
    .controller('UploadDesignController', ['$scope', function ($scope) {

        //Search for user with name or id.
        //name
        $scope.searchUserWithName = function () {


            if ($scope.searchWithNameInput) {
               
                var loweredInputText = $scope.searchWithNameInput.toLowerCase(); //seperate function because cannot convert id to lowercase


                var userQuery = new Parse.Query(Parse.User);
                userQuery.contains('firstName', loweredInputText);

                userQuery.find({
                    success: function (results) {

                        console.log(results);

                        $scope.users = results;
                        $scope.$apply();
                        
                    }
                    , error: function (error) {
                        alert(error.message);
                    }
                });

            } else {
                console.log('put something man!')
            }
   
        }
        //id
        $scope.searchUserWithId = function () {
           
            if ($scope.searchWithIdInput) {
                var id = $scope.searchWithIdInput;

                var userQuery = new Parse.Query(Parse.User);
                userQuery.contains('objectId', id);

                userQuery.find({
                    success: function (results) {

                        console.log(results);

                        $scope.users = results;
                        $scope.$apply();

                    }
                    , error: function (error) {
                        alert(error.message);
                    }
                });

            } else {
                console.log('put something man!')
            }
        }

        //Select user
        $scope.selectUser = function (user) {

            //Show user information to confirm selectd user.
            $scope.selectedUser = user;
            $scope.selectedUser.firstName = user.get('firstName');
            $scope.selectedUser.lastName = user.get('lastName');
            $scope.selectedUser.email = user.get('email');

        }

        $scope.uploadToServer = function () {

            var filesChosen = document.getElementById("filesChosen").files;

            var designer = $scope.selectedUser;
            var designerName = designer.get('firstName');
            var collectionName = $scope.collectionName;
            var fixedSearchIndex = designerName.toLowerCase() + " " + collectionName.toLowerCase();
            var variedSearchIndex = ["new","popular"];
            console.log(designer);

            var DesignCollections = Parse.Object.extend("DesignCollections");
            var newCollection = new DesignCollections();

            newCollection.set('collectionName', collectionName);
            newCollection.set('createdBy', designer);
            newCollection.set('fixedSearchIndex', fixedSearchIndex);
            newCollection.set('variedSearchIndex', variedSearchIndex);
            
            //var imagePreview1 = new Parse.File("imagePreview1.png", imagesChosen[0], "image/png");
            //var imagePreview2 = new Parse.File("imagePreview2.png", imagesChosen[1], "image/png");
            //var imagePreview3 = new Parse.File("imagePreview3.png", imagesChosen[2], "image/png");
            var imageUploadCounter = 0;

            newCollection.save(null, {
                success: function (newCollection) {
                    alert('New collection created!')

                    var images = [];
                    
                    for (i = 0; i < filesChosen.length; i++) {

                        //get file URL 
                        var file = filesChosen[i];
                        var fileURL = window.URL.createObjectURL(file);

                        console.log(file.name);

                        //create image var to contain src of file
                        var image = new Image();
                        image.src = fileURL;
                        image.crossOrigin = "Anonymous";

                        images[i] = image;
                        

                        image.onload = (function (value) { //This is complicated: see http://stackoverflow.com/questions/7641588/if-i-load-multiple-images-with-the-for-loop-do-i-only-need-one-img-onload-funct for reference
                            return function () {
                                console.log('uploading... ' + images[value].src);
                                    var canvas = document.createElement('CANVAS');
                                    canvas.height = images[value].height;
                                    canvas.width = images[value].width;
                                    var ctx = canvas.getContext('2d');
                                    ctx.drawImage(images[value], 0, 0);
                                    imageDataViaCanvas = canvas.toDataURL();


                                    //Creating parse file to upload file with parse sdk.
                                    var parseImage = new Parse.File(file.name, { base64: imageDataViaCanvas });

                                    var DesignData = Parse.Object.extend('DesignData');
                                    var newDesignData = new DesignData();

                                    newDesignData.set('imageFile', parseImage);
                                    newDesignData.set('inCollection', newCollection);
                                    newDesignData.set('byDesigner', designer);
                                 
                                    
                                    newDesignData.save(null, {
                                        
                                        success: function (newDesignData) {

                                            console.log('upload ' + value + "th succses!");
                                            
                                            if (imageUploadCounter < 3) { //upload 3 first image uploaded as preview image
                                                var index = 1 + imageUploadCounter;
                                                console.log('imagePreview' + index);
                                                console.log(imageUploadCounter);
                                                newCollection.set('preview' + index, newDesignData);

                                            }

                                            imageUploadCounter++;
             
                                            if (imageUploadCounter == 3) {//when obtaining 3 images for preview then save

                                                newCollection.save();
                                            }


                                        }, error(error) {

                                            console.log('error upload'+ file.name + ': ' + error.message)
                                            console.log(error)
                                        }
                                    });

                            }
                        })(i);


                    }


                }, error: function (error) {
                    alert(error.message);
                }
            });
            
        }

        //GET METHODS
        $scope.getUserName = function (user) {
            
            return user.get('username');
        }





    }])