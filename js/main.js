/// <reference path="../partials/about.html" />
/**
 * Main AngularJS Web Application
 */

//Initialize parse with app key
Parse.initialize("WhAmgtyQMIGrpkP6ITS0J7Wh72HHUAFMTESWzEdf", "fIj6ZlJWPLwVyU9mddIwFNiZUYQEifk3InbpAi6i");
//Parse.serverURL = 'http://localhost:1337/parse'; //Diriect the sdk to connect to local server (for now), this will match .lister on server.js
Parse.serverURL = 'http://parseserver-updj2-env.ap-southeast-1.elasticbeanstalk.com/parse';

//Facebook SDK  
window.fbAsyncInit = function () {
    //FB.init({
    //    appId      : '164265673961160',
    //    xfbml      : true,
    //    version    : 'v2.5'
    //});

    //Replace with facebook utils from ParseSDK
    Parse.FacebookUtils.init({ // this line replaces FB.init({
        appId: '164265673961160', // Facebook App ID
        status: true,  // check Facebook Login status
        cookie: true,  // enable cookies to allow Parse to access the session
        xfbml: true,  // initialize Facebook social plugins on the page
        version: 'v2.5', // point to the latest Facebook Graph API version

    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//Set keys for omise.js
Omise.setPublicKey("pkey_test_52uwdobi5bmzhp03pxr");


angular.module('yourTshirtDesign', [
  'ngRoute',
  'ngCookies',
  'ui.bootstrap'

]);

var app = angular.module('yourTshirtDesign');

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      // Home
      .when("/", { templateUrl: "partials/home.html", controller: "PageCtrl" })
      // Pages
      .when("/about", { templateUrl: "partials/about.html", controller: "PageCtrl" })
      .when("/faq", { templateUrl: "partials/faq.html", controller: "PageCtrl" })
      .when("/pricing", { templateUrl: "partials/pricing.html", controller: "PageCtrl" })
      .when("/services", { templateUrl: "partials/services.html", controller: "PageCtrl" })
      .when("/contact", { templateUrl: "partials/contact.html", controller: "PageCtrl" })
      .when("/canvas", { templateUrl: "partials/canvas.html", controller: "PageCtrl" })
      .when("/designer", { templateUrl: "partials/designer.html", controller: "PageCtrl" })
      .when("/profile", { templateUrl: "partials/profile.html", controller: "PageCtrl" })
      .when("/cart", { templateUrl: "partials/cart.html", controller: "PageCtrl" })
      .when("/manageorder", { templateUrl: "partials/manageorder.html", controller: "PageCtrl" })
         .when("/thankyou", { templateUrl: "partials/thankyou.html", controller: "PageCtrl" })
      // Blog
      .when("/blog", { templateUrl: "partials/blog.html", controller: "BlogCtrl" })
      .when("/blog/post", { templateUrl: "partials/blog_item.html", controller: "BlogCtrl" })
      // else 404
      .otherwise("/404", { templateUrl: "partials/404.html", controller: "PageCtrl" });
}]);


/**
 * Controls all other Pages
 */
app.controller('PageCtrl', function (/* $scope, $location, $http */) {
    console.log("Page Controller reporting for duty.");

    // Activates the Carousel
    $('.carousel').carousel({
        interval: 5000
    });

    // Activates Tooltips for Social Links
    $('.tooltip-social').tooltip({
        selector: "a[data-toggle=tooltip]"
    })
});


app.directive('bindValueTo', function () {
    return {
        restrict: 'A',

        link: function ($scope, $element, $attrs) {

            var prop = capitalize($attrs.bindValueTo),
                getter = 'get' + prop,
                setter = 'set' + prop;

            $element.on('change keyup select', function () {
                $scope[setter] && $scope[setter](this.value);
            });

            $scope.$watch($scope[getter], function (newVal) {
                if ($element[0].type === 'radio') {
                    var radioGroup = document.getElementsByName($element[0].name);
                    for (var i = 0, len = radioGroup.length; i < len; i++) {
                        radioGroup[i].checked = radioGroup[i].value === newVal;
                    }
                }
                else {
                    $element.val(newVal);
                }
            });
        }
    };
});

app.directive('objectButtonsEnabled', function () {
    return {
        restrict: 'A',

        link: function ($scope, $element, $attrs) {
            $scope.$watch($attrs.objectButtonsEnabled, function (newVal) {

                $($element).find('.btn-object-action')
                  .prop('disabled', !newVal);
            });
        }
    };
});