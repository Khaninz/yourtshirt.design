﻿angular.module('yourTshirtDesign')
.controller('paymentController', function ($scope, $http) {

    var currentUser = Parse.User.current();
    $scope.isCartEmpty = true;
    $scope.isSaveEmpty = true;

    var serverURL = 'http://parseserver-updj2-env.ap-southeast-1.elasticbeanstalk.com';

    if (currentUser) {//If use is logged on

        //get items in cart
        var ItemsInCart = Parse.Object.extend("ItemsInCart");
        var query = new Parse.Query(ItemsInCart);
        query.equalTo('createdBy', currentUser); //by current user
        query.equalTo('saveForLater', false); // is not saved for later
        query.equalTo('isOrdered', false); // item that is already oredred will not show.
        query.include('selectedShirtDesign'); 
        query.find({
            success: function (itemsInCart) {

                $scope.itemsInCart = itemsInCart;
                $scope.getCheckOutPrice($scope.itemsInCart);
                $scope.checkCartEmpty();
                $scope.$apply();

                //console.log($scope.itemsInCart);
            }, error: function (error) {
                console.log(error.message);
            }
        });

        //get items that save for later.
        var querySavedItems = new Parse.Query(ItemsInCart);
        querySavedItems.equalTo('createdBy', currentUser); //by current user
        querySavedItems.equalTo('saveForLater', true); // is  saved for later
        querySavedItems.equalTo('isOrdered', false); // item that is already oredred will not show.
        querySavedItems.include('selectedShirtDesign');
        querySavedItems.find({
            success: function (itemsSavedForlater) {

                $scope.itemsSavedForLater = itemsSavedForlater;
                $scope.checkSaveEmpty();
                $scope.$apply();

            }, error: function (error) {
                console.log(error.message);
            }
        });

        //get Address
        var Addresses = Parse.Object.extend("Addresses");
        var addressQuery = new Parse.Query(Addresses);
        addressQuery.equalTo('createdBy', currentUser);

        addressQuery.find({
            success: function (results) {

                $scope.userAddress = results;

                $scope.$apply();

                console.log($scope.itemsInCart);
            }, error: function (error) {

            }
        });

        //get omise customer id,
        var omiseID = currentUser.get('OmiseID')
        if (omiseID) {
            //use omise ID to retrieve cards.
            console.log('I have Omise id!')

            $http({ //create new customer via omise on node server
                method: "GET",
                url: serverURL+ "/payment/retrievecards" + "/" + omiseID //This address will be changed acoordingly to its domain.


            }).then(function mySucces(cardList) {
                console.log('get card list success');
                console.log(cardList);
                console.log(cardList.data.data.length);

                $scope.userCards = cardList.data.data;

            }, function myError(response) {
                console.log('retrieve card failed');
            });

        } else {
            console.log('I do not have ID');
            //automatically create one for user.
            var email = currentUser.get('username');
            console.log(email);

            $http({ //create new omise customer via omise on node server
                method: "GET",
                url: serverURL + "/payment/create" + "/" + email //This address will be changed acoordingly to its domain.


            }).then(function mySucces(customer) {
                //create new customer success
                console.log('create customer success');
                console.log(customer)
                console.log(customer.data.id);

                currentUser.set('OmiseID', customer.data.id);

                currentUser.save(null, { //save omise customer id to current user
                    success: function (currentUser) {
                        console.log('save omise id success!');
                    },
                    error: function (currentUser, error) {
                        console.log('failed to save omise id: ' + error.message);
                    }
                });

            }, function myError(response) {
                console.log('Error');
                console.log(response)
            });

        }

    } else {// if user has not logged on


    }


    $scope.saveAddress = function () {

        var Addresses = Parse.Object.extend("Addresses");
        var address = new Addresses();

        address.set('createdBy', Parse.User.current());
        address.set('receiverFullName', $scope.newReceiverFullName);
        address.set('addressLine1', $scope.newAddressLine1);
        address.set('addressLine2', $scope.newAddressLine2);
        address.set('addressCity', $scope.newAddressCity);
        address.set('addressProvince', $scope.newAddressProvince);
        address.set('addressPostCode', $scope.newAddressPostCode);

        address.save(null, {
            success: function (address) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + address.id);
                $scope.userAddress.push(address);
                //clear field
                $scope.newReceiverFullName = "";
                $scope.newAddressLine1= "";
                $scope.newAddressLine2= "";
                $scope.newAddressCity= "";
                $scope.newAddressProvince = "";
                $scope.newAddressPostCode = "";

                $scope.$apply();
            },
            error: function (address, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });

    }

    $scope.getFullAddress = function (address) {

        var fullName = address.get('receiverFullName');
        var line1 = address.get('addressLine1');
        var line2 = address.get('addressLine2');
        var city = address.get('addressCity');
        var province = address.get('addressProvince');
        var postCode = address.get('addressPostCode');

        return fullName + ": " + line1 + " " + line2 + " " + city + " " + province + " " + postCode;
    }

    $scope.selectAddress = function (address) {
        $scope.selectedAddress = address;
        $('#addAddressModal').modal('toggle')
    }

    $scope.addCreditCard = function () {

        var omiseID = currentUser.get('OmiseID');

        var card = {
            "name": $scope.newCardHolderName,
            "number": $scope.newCardNumber,//"4242424242424242", //number for card testing.
            "expiration_month": $scope.newCardExpirationMonth,//"9",
            "expiration_year": $scope.newCardExpirationYear,//"2018",
            "security_code": $scope.cardSecurityCode//"123"
        }

        Omise.createToken("card", card, function (statusCode, response) {
            if (statusCode == 200) {
                // Success: send back the TOKEN_ID to your server. The TOKEN_ID can be
                // found in `response.id`.
                var token = response.id;

                alert('card verified!')

                $http({ //attach card to customer using token and omise id.
                    method: "GET",
                    url: serverURL + "/payment/attachcard" + "/" + omiseID + "/" + token //This address will be changed acoordingly to its domain.

                }).then(function mySucces(customerResponse) {
                    console.log('add card success')
                    console.log(customerResponse)
                    console.log(customerResponse.data);
                    
                    $scope.userCards = customerResponse.data.cards.data;

                }, function myError(response) {
                    console.log('add card failed')
                    console.log(response)
                });


            } else {
                // Error: display an error message. Note that `response.message` contains
                // a preformatted error message. Also note that `response.code` will be
                // "invalid_card" in case of validation error on the card.
                alert('card is not verified: ' + response.message);
            };
        });
    }

    $scope.selectCard = function (card) {
        $scope.selectedCard = card;
        $('#addPaymentModal').modal('toggle');
    }

    $scope.reviewOrder = function () {
        if ($scope.selectedAddress && $scope.selectedCard) {
            //both are selected
            $scope.toggleModal('#reviewConfirmModal');
        } else {
            alert('Please select card and address.');
        }
    }

    $scope.placeOrder = function () {

        //create order
        var Orders = Parse.Object.extend("Orders");
        var order = new Orders();
        var itemsInCart = $scope.itemsInCart;
        var amount = $scope.checkOutPrice;
        var address = $scope.selectedAddress;

        order.set('itemsInOrder', itemsInCart);
        order.set('checkOutPrice', amount);
        order.set('createdBy', currentUser);
        order.set('deliverTo', address);
        order.set('status_deliver', false);
        order.set('status_paid', false);

        order.save(null, {
            success: function (order) {
                // Execute any logic that should take place after the object is saved.
                alert('New object created with objectId: ' + order.id);
                
                //charge
                var orderID = order.id
                var omiseID = currentUser.get('OmiseID');
                var cardID = $scope.selectedCard.id;


                $http({ //attach card to customer using token and omise id.
                    method: "GET",
                    url: serverURL + "/payment/placeorder" + "/" + omiseID + "/" + cardID + "/" + amount + "/" + orderID//This address will be changed acoordingly to its domain.

                }).then(function success(response) {


                    alert('payment success');
                    console.log(response);
                    //update order to paid
                    order.set('status_paid', true);
                    order.save();
                    alert('order updated');
                    //update each items in cart to be ordered.

                    var shirtDesignArray = [];

                    //console.log(itemsInCart);
                    for (i = 0; i < itemsInCart.length; i++) {

                        //to remove items in cart out from the cart
                        var itemInCart = itemsInCart[i];
                        itemInCart.set('isOrdered', true);
                        //console.log(itemsInCart[i].id);

                        //lock shirt design from modifying, for tracking purpose.
                        var shirtDesign = itemInCart.get('selectedShirtDesign');
                        shirtDesign.set('isLocked', true);
                        //console.log(shirtDesign.id);
                        shirtDesignArray.push(shirtDesign);
                    }

                    var parseObjects = [];
                    parseObjects.push.apply(parseObjects, itemsInCart);
                    parseObjects.push.apply(parseObjects, shirtDesignArray);

                    Parse.Object.saveAll(parseObjects, {
                        success: function (objs) {
                            console.log(objs);
                        },
                        error: function (error) {
                            console.log(error.message);
                        }
                    });

                    //remove modal
                    $scope.toggleModal('#reviewConfirmModal');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    //redirect user to order completed with navigation to track order.
                    window.location.assign("#/thankyou");

                }, function error(response) {

                });

            },
            error: function (order, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('Failed to create new object, with error code: ' + error.message);
            }
        });



    }

    $scope.saveForLater = function (item,$index) {
       
        //save item status
        item.set('saveForLater', true);
        item.save(null,{
            success:function(item){//success
                
                
                $scope.$apply(function(){
                    //remove item form incart array
                    $scope.itemsInCart.splice($index, 1);
                    //push item into save for later array.
                    $scope.itemsSavedForLater.unshift(item);
                    //update checkout price
                    $scope.getCheckOutPrice($scope.itemsInCart);
                    //update State
                    $scope.checkCartEmpty();
                    $scope.checkSaveEmpty();
                });
                
            }
            ,error: function(){
            
            }
        });
        
    }

    $scope.moveToCart = function (item,$index) {
        //save item status
        item.set('saveForLater', false);
        item.save(null, {
            success: function (item) {//success


                $scope.$apply(function () {
                    //remove item form save for later array
                    $scope.itemsSavedForLater.splice($index, 1);
                    //push item into in cart items
                    $scope.itemsInCart.push(item);
                    //update checkout price
                    $scope.getCheckOutPrice($scope.itemsInCart);
                    //update State
                    $scope.checkCartEmpty();
                    $scope.checkSaveEmpty();
                });

            }
            , error: function () {

            }
        });
    }

    $scope.deleteCard = function (card ,$index) {

        var omiseID = currentUser.get('OmiseID');
        var cardID = card.id;

        $http({ //delete card
            method: "GET",
            url: serverURL + "/payment/deletecard" + "/" + omiseID + "/" + cardID //This address will be changed acoordingly to its domain.

        }).then(function mySucces(cardResponse) {
            console.log('delete card success')
            console.log(cardResponse)
            console.log(cardResponse.data);

            $scope.userCards.splice($index, 1);

        }, function myError(response) {
            console.log('delete card failed')
            console.log(response)
        });


    }

    $scope.deleteAddress = function (address,$index) {
        address.destroy({
            success: function (address) {
                $scope.userAddress.splice($index, 1);
                $scope.$apply();
            },
            error: function (address,error) {

            }
        });
    }
    

    //-----------------------------------------------------------------------------------------------------------------------
    //UTILS
    $scope.toggleModal = function (modalID) {
        $(modalID).modal('toggle');
    }

    $scope.checkCartEmpty = function () {
        if ($scope.itemsInCart.length == 0) {
            $scope.isCartEmpty = true;
        } else {
            $scope.isCartEmpty = false;
        }
    }

    $scope.checkSaveEmpty = function () {
        if ($scope.itemsSavedForLater.length == 0) {
            $scope.isSaveEmpty = true;
        } else {
            $scope.isSaveEmpty = false;
        }
    }

    //GET METHODS
    $scope.getTotalPrice = function (itemInCart) {
        return itemInCart.get('totalPrice');
    }

    $scope.getItemTypeBackground = function (itemInCart) {
        var shirtDesign = itemInCart.get('selectedShirtDesign');
        var item = shirtDesign.get('item');
        var color = shirtDesign.get('color');

        var backgroundURL = 'url(../img/items/' + item + '/front-' + color + '.png)'
        return backgroundURL;
    }

    $scope.getDesignImageURL = function (itemInCart) {
        var shirtDesign = itemInCart.get('selectedShirtDesign');
        var image = shirtDesign.get('thumbnail')
        var imageURL = image.url();
        return imageURL;
    }

    $scope.getDesignName = function (itemInCart) {
        var shirtDesign = itemInCart.get('selectedShirtDesign');
        var designName = shirtDesign.get('designName');
        return designName;
    }

    $scope.getItemType = function (itemInCart) {
        var shirtDesign = itemInCart.get('selectedShirtDesign');
        var itemType = shirtDesign.get('item');
        return itemType;
    }

    $scope.getColor = function (itemInCart) {
        var shirtDesign = itemInCart.get('selectedShirtDesign');
        var itemColor = shirtDesign.get('color');
        return itemColor;
    }

    $scope.getCheckOutPrice = function (itemsInCartArray) {
        var checkOutPrice = 0;
        for (i = 0; i < itemsInCartArray.length; i++) {
            var priceTxt = itemsInCartArray[i].get('totalPrice');
            var priceInt = parseInt(priceTxt);

            checkOutPrice = checkOutPrice + priceInt;

        }

        $scope.checkOutPrice = checkOutPrice;
    }

    $scope.getSizeS = function (itemInCart) {
        return itemInCart.get('sizeS');
    }
    $scope.getSizeM = function (itemInCart) {
        return itemInCart.get('sizeM');
    }
    $scope.getSizeL = function (itemInCart) {
        return itemInCart.get('sizeL');
    }
    $scope.getSizeXL = function (itemInCart) {
        return itemInCart.get('sizeXL');
    }
    $scope.getSizeXXL = function (itemInCart) {
        return itemInCart.get('sizeXXL');
    }
});