﻿//CONTROLLER TO CONTROL COLLECTION PART
angular.module('yourTshirtDesign')
    .controller('CollectionController', ['$http', '$scope', function ($http, $scope, httpService) {

        //init search collection
        $scope.searchText = "";
        $scope.searchSelection = "All";

        //$scope used in this controller 
        //$scope.collectionDataArray : For putting array of collections
        //$scope.collectionDetailArray : For putting detail of collection chosen
        //$scope.isShowDetail : use for indicate state wheter, to show detail page or collection page.
        //$scope.detailedCollectionName = "";
        //$scope.selectedCollectionDesigner = "";
        //$scope.moreDesignArray = [];
        //$scope.searchResultText = "";
        //$scope.shirtDesignArray = [];

        //START: DECLARE FUNCTION TO BE USED WHEN APP START, this cannot be put after $scope.startSearch();
        var collectionSearch = function (text, selection) {

            var loweredText = text.toLowerCase();
            var loweredSelection = selection.toLowerCase();

            console.log(loweredText);
            console.log(loweredSelection);

            var DesignCollections = Parse.Object.extend("DesignCollections");
            var query = new Parse.Query(DesignCollections);
            //if nothing input seach all, JUST FOR NOW.
            if (loweredText == "") {
                //do not put any constrain
            } else {
                query.contains("fixedSearchIndex", loweredText);
            }

            //constrain with the selection
            if (loweredSelection == 'all') {
                //do not put any constrain
            } else {
                query.contains("variedSearchIndex", loweredSelection);
            }

            query.include("createdBy"); //to include user object associate with collection, no need to query for user again.
            query.include("preview1");
            query.include("preview2");
            query.include("preview3");

            query.find({
                success: function (results) {


                    console.log("Query success with " + results.length + " results");
                    querySuccess(results, $scope.searchText);

                }, error: function (error) {
                    queryFailed(error);
                }

            });
        }

        var querySuccess = function (results, text) {

            console.log(results);
            $scope.$apply(function () {
                $scope.collectionDataArray = results;

            });
            console.log($scope.collectionDataArray);

            console.log("data loaded with" + results.length);

            //if succes but no result found show 'result not found'
            if ($scope.collectionDataArray.length == 0) {
                $scope.$apply(function () {
                    $scope.searchResultText = text;

                });
            }
        }

        var queryFailed = function (error) {
            alert(error.message);
        }


        //Seach function when a search button is clicked. And when first initailize;
        $scope.startSearch = function () {
            console.log("Search start! with " + $scope.searchText + $scope.searchSelection);
            collectionSearch($scope.searchText, $scope.searchSelection);
            $scope.isShowDetail = false;
        }

        //END: DECLARE FUNCTION TO BE USED WHEN APP START, this cannot be put after $scope.startSearch();

        //init first search when page load
        $scope.startSearch();

        //back just by changing state and link with ng-hide/show
        $scope.backToList = function () {
            $scope.isShowDetail = false;
        }

        //function when collection is clicked!
        $scope.showDetail = function (collection) {
            $scope.isShowDetail = true;

            $scope.detailedCollectionName = $scope.getCollectionName(collection);
            $scope.selectedCollectionDesigner = $scope.getCollectionDesignerName(collection);

            showSelectedCollection(collection);
            showMoreDesign(collection);

        }

        //fucntion after collection is clicked.
        var showSelectedCollection = function (collection) {

            var DesignData = Parse.Object.extend("DesignData");
            var query = new Parse.Query(DesignData);

            query.contains('inCollection', collection.id);

            query.find({
                success: function (results) {
                    console.log(results);
                    $scope.$apply(function () {
                        $scope.collectionDetailArray = results;

                    });

                }, error: function (error) {
                    queryFailed();
                }

            });
        }

        //function to query more deisng by designer, but not in selected collection, when the collection is clicked.
        var showMoreDesign = function (collection) {

            var DesignData = Parse.Object.extend("DesignData");
            var query = new Parse.Query(DesignData); //Main query

            var DesignCollections = Parse.Object.extend("DesignCollections");
            var queryToExclude = new Parse.Query(DesignCollections);
            queryToExclude.contains('objectId', collection.id); //SubQuery

            var designer = collection.get('createdBy');
            var designerId = designer.id;

            console.log(designer);
            console.log(designerId);

            query.contains("byDesigner", designerId); //Constrain main query for all design with design id.

            query.doesNotMatchQuery("inCollection", queryToExclude);//remove results that has a collection in sub query.

            query.find({
                success: function (results) {
                    console.log(results);
                    $scope.$apply(function () {
                        $scope.moreDesignArray = results;

                    });

                }, error: function (error) {
                    queryFailed();
                }

            });

        }

        //function to add image to canvas, with ng-click
        //ถ้าหากเป็นไฟล์ที่มาจาก http://files.parsetfss จะใช้ไม่ได้
        //ต้องเป็นไฟล์ที่อัพโหลดคขึ้นใหม่ผ่าน local server หรือ server เราเองในอนาคตถึงจะใช้ได้
        $scope.addImageFromCollection = function (design) {

            var image = design.get('imageFile');
            var imageURL = image.url()

            fabric.Image.fromURL(imageURL, function (image) {
                //frontCanvas.add(image);
                image.scale(0.5);
                canvas.add(image);
            }, { "crossOrigin": "anonymous" });

        }

        //GET METHODS
        $scope.getCollectionDesignerName = function (parseCollectionObject) {

            var designer = parseCollectionObject.get("createdBy"); //This require includeKey in search '.include'
            var designerName = designer.get('firstName');

            return designerName;

        }

        $scope.getCollectionName = function (parseCollectionObject) {
            var collectionName = parseCollectionObject.get("collectionName");
            return collectionName;
        }

        //Later work, to get preview images and view in each collection.
        //$scope.getPreviewImages = function (parseCollectionObject,imagesContainer) {
        //    console.log('init working!');

        //    var collectionId = parseCollectionObject.id;
        //    console.log(collectionId);

        //    var DesignData = Parse.Object.extend("DesignData");
        //    var query = new Parse.Query(DesignData);
        //    query.contains("inCollection", collectionId);
        //    query.limit(3);

        //    query.find({
        //        success: function (results) {
        //            imagesContainer = results;
        //            $scope.$apply();
        //            return imagesContainer;
        //        }, error: function (error) {
        //           console.log('there is an error getting preview images')
        //        }
        //    })

        //}
        //For now use hardcode dull method below.
        $scope.getPreviewImage1URL = function (parseCollectionObject) {
            var designData = parseCollectionObject.get('preview1');
            var image = designData.get("imageFile");
            var imageURL = image.url();
            return imageURL;
        }

        $scope.getPreviewImage2URL = function (parseCollectionObject) {
            var designData = parseCollectionObject.get('preview2');
            var image = designData.get("imageFile");
            var imageURL = image.url();
            return imageURL;
        }

        $scope.getPreviewImage3URL = function (parseCollectionObject) {
            var designData = parseCollectionObject.get('preview3');
            var image = designData.get("imageFile");
            var imageURL = image.url();
            return imageURL;
        }


        $scope.getImageURL = function (designData) {
            var image = designData.get("imageFile");
            var imageURL = image.url();
            return imageURL;
        }

    }]);

angular.module('yourTshirtDesign').controller('CanvasControls', function ($scope, $http) {

    //$scope.getActiveStyle = getActiveStyle;

    addAccessors($scope);
    watchCanvas($scope);

    CanvasControls = this;

    //Variables to be used for canvas
    $scope.designName = 'My Design';
    $scope.activeSide = 'front';
    $scope.activeColor = 'white';
    $scope.activeItem = 'tshirt';
    $scope.isFrontActive = true;
    $scope.frontCanvasJSON;
    $scope.backCanvasSJON;
    $scope.activeShirtDesign;

    //Arrays of shirtDesign
    $scope.shirtsDesignedByUser;

    //Object holder
    $scope.selectedShirtDesign; //use when adding item to cart.



    //query for shirt already design by user
    if (Parse.User.current()) {
        var ShirtDesign = Parse.Object.extend("ShirtDesign");

        var query = new Parse.Query(ShirtDesign);
        query.equalTo("createdBy", Parse.User.current());
        query.descending('updatedAt');
        query.find({
            success: function (results) {

                $scope.$apply(function () {
                    $scope.shirtsDesignedByUser = results;

                });
                console.log('There are ' + results.length + " design by user.")
                console.log(results);

            }, error: function (error) {

            }
        });
    }


    $scope.colors = function (color) {
        var view = $scope.activeSide;
        var item = $scope.activeItem;
        $scope.activeColor = color;
        $('#shirt-position').css('background-image', 'url(../img/items/' + item + '/' + view + '-' + color + '.png)');

    }


    $scope.changeview = function (view) { //is called when switching between front and back.

        console.log('change view!');

        $scope.activeSide = view;

        $scope.colors($scope.activeColor);

        if (view == 'front') {
            $scope.isFrontActive = true;
            //switching to front

            //save backcanvas
            $scope.backCanvasJSON = JSON.stringify(canvas.toJSON());

            //clear canvas
            canvas.clear().renderAll;
            //load front canvas            
            canvas.loadFromJSON($scope.frontCanvasJSON, function () {

                canvas.renderAll();
                CanvasControls.UnBlockUI();
            });

        } else {
            $scope.isFrontActive = false;

            //switching to back
            //save front canvas
            $scope.frontCanvasJSON = JSON.stringify(canvas.toJSON());

            //clear canvas
            canvas.clear().renderAll();
            //load back canvas            
            canvas.loadFromJSON($scope.backCanvasJSON, function () {

                canvas.renderAll();
                CanvasControls.UnBlockUI();
            });


        }

    }

    $scope.colors('white'); //init view with initial params

    //function of save button
    $scope.saveCanvas = function () {

        //if user not log in, user must login first to save
        if (Parse.User.current()) {

            //check if any shirt design is active
            if ($scope.activeShirtDesign) {//if active: save to active

                var shirtDesign = $scope.activeShirtDesign;

                if ($scope.activeSide == 'front') {
                    //save canvas to front 
                    $scope.frontCanvasJSON = JSON.stringify(canvas.toJSON());
                    //only save front because back will be stored during switching.
                }

                if ($scope.activeSide == 'back') {
                    $scope.backCanvasJSON = Json.stringify(canvas.toJSON());
                    //only save back as front will be stored during switching.
                }

                var png = canvas.toDataURL(); //canvas should be front first.
                var file = new Parse.File("thumnailImage.png", { base64: png });

                shirtDesign.set("designName", $scope.designName);
                shirtDesign.set("frontCanvasJSON", $scope.frontCanvasJSON);
                shirtDesign.set("backCanvasJSON", $scope.backCanvasJSON);
                shirtDesign.set("item", $scope.activeItem);
                shirtDesign.set("color", $scope.activeColor);
                shirtDesign.set("price", 390); //temporary
                shirtDesign.set("thumbnail", file);

                shirtDesign.save(null, {
                    success: function (shirtDesign) {
                        // Execute any logic that should take place after the object is saved.
                        alert('Save item success: ' + shirtDesign.id);
                        $scope.$apply(function () {
                            $scope.activeShirtDesign = shirtDesign;
                            $scope.shirtDesignedByUser.unshift(shirtDesign); //doesn't work as expected.
                        });
                    },
                    error: function (shirtDesign, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        alert('Failed to save: ' + error.message);
                    }
                });

            } else { //else: create new object and save.

                if ($scope.activeSide == 'front') {
                    //save canvas to front 
                    $scope.frontCanvasJSON = JSON.stringify(canvas.toJSON());
                    //only save front because back will be stored during switching.
                }

                if ($scope.activeSide == 'back') {
                    $scope.backCanvasJSON = Json.stringify(canvas.toJSON());
                    //only save back as front will be stored during switching.
                }

                var ShirtDesign = Parse.Object.extend("ShirtDesign");
                var shirtDesign = new ShirtDesign();

                //create file for thumbnail
                var png = canvas.toDataURL();
                var file = new Parse.File("thumnailImage.png", { base64: png });

                shirtDesign.set("createdBy", Parse.User.current());
                shirtDesign.set("designName", $scope.designName);
                shirtDesign.set("frontCanvasJSON", $scope.frontCanvasJSON);
                shirtDesign.set("backCanvasJSON", $scope.backCanvasJSON);
                shirtDesign.set("item", $scope.activeItem);
                shirtDesign.set("color", $scope.activeColor);
                shirtDesign.set("price", 390); //temporary
                shirtDesign.set("thumbnail", file);

                shirtDesign.save(null, {
                    success: function (shirtDesign) {
                        // Execute any logic that should take place after the object is saved.
                        alert('New object created with objectId: ' + shirtDesign.id);
                        //update object to view
                        $scope.$apply(function () {
                            $scope.shirtDesignedByUser.unshift(shirtDesign); //doesn't work as expected.
                            $scope.activeShirtDesign = shirtDesign;

                        });
                    },
                    error: function (shirtDesign, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        alert('Failed to create new object, with error code: ' + error.message);
                    }
                });
            }


        } else {
            alert('Please log in first to save');
        }


    }

    /***********************************************************LOAD DESIGN*************************************************************************/
    $scope.loadDesign = function (shirtDesign) {

        var item = shirtDesign.get("item");
        var color = shirtDesign.get("color");
        var frontCanvasJSON = shirtDesign.get("frontCanvasJSON");
        var backCanvasJSON = shirtDesign.get("backCanvasJSON");
        var designName = shirtDesign.get("designName");

        //load params to active objects
        $scope.activeSide = 'front';
        $scope.activeColor = color;
        $scope.activeItem = item;
        $scope.frontCanvasJSON = frontCanvasJSON;
        $scope.backCanvasJSON = backCanvasJSON;

        $scope.colors($scope.activeColor);//change background item & color

        canvas.clear().renderAll;//clear canvas

        canvas.loadFromJSON($scope.frontCanvasJSON, function () { //load front canvas

            canvas.renderAll();

        });

        $scope.activeShirtDesign = shirtDesign;//after load set $scope.activeShirtDesign to loaded object.


    };

    $scope.createNewDesign = function () {
        //when creating new design, set $scope.activeShirtDesign to none. 
        //so when trying to save, it will detect empty object and save as a new one. also set $scope.activeShirtDesign to object.
    }

    /************************************************************************************************************************************/


    this.BlockUI = function () {
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '0',
                backgroundColor: '#fff',
                opacity: 0.5,
                color: '#000'
            }
        });
    }
    this.UnBlockUI = function () {
        $.unblockUI();
    }

    $scope.tempToPNG = function () { //for testing only

        var testPNG = canvas.toDataURL();

        var file = new Parse.File("testImage.png", { base64: testPNG });

        var TestImage = Parse.Object.extend("TestImageFromCanvas");
        var testImage = new TestImage();

        testImage.set("testPNG", file);

        testImage.save(null, {
            success: function (testImage) {
                alert('Test image is saved! in PNG success!')
            }, error() {

            }
        })
    }

    //get methods
    $scope.getDesignImageURL = function (shirtDesign) {
        try {
            var image = shirtDesign.get('thumbnail')
            var imageURL = image.url();
            return imageURL;
        } catch (error) {
            console.log(error.message);
        }
        
    }

    $scope.getPrice = function (shirtDesign) {
        try {
            return shirtDesign.get('price');
        } catch (error) {
            console.log(error.message);
        }
       
    }

    $scope.getItem = function (shirtDesign) {
        try {
            var item = shirtDesign.get('item');
            var color = shirtDesign.get('color');

            var backgroundURL = 'url(../img/items/' + item + '/front-' + color + '.png)'
            return backgroundURL;
        } catch (error) {
            console.log(error.message);
        }

    }

    $scope.selectShirtDesign = function (shirtDesign) {
        $scope.selectedShirtDesign = shirtDesign;
        alert($scope.selectedShirtDesign);
    }

    $scope.addItemsToCart = function () {

        //TO-DO 
        //if item already in cart update it
        //if not create new

        var ItemsInCart = Parse.Object.extend("ItemsInCart");
        var items = new ItemsInCart();

        var sizeS = parseInt($scope.sizeS);
        var sizeM = parseInt($scope.sizeM);
        var sizeL = parseInt($scope.sizeL);
        var sizeXL = parseInt($scope.sizeXL);
        var sizeXXL = parseInt($scope.sizeXXL);

        var price = $scope.selectedShirtDesign.get('price');
        var total = sizeS + sizeM + sizeL + sizeXL + sizeXXL;
        var totalPrice = price * total;

        items.set('createdBy', Parse.User.current());
        items.set('selectedShirtDesign', $scope.selectedShirtDesign);
        items.set('sizeS', sizeS);
        items.set('sizeM', sizeM);
        items.set('sizeL', sizeL);
        items.set('sizeXL', sizeXL);
        items.set('sizeXXL', sizeXXL);
        items.set('saveForLater', false);
        items.set('totalPrice', totalPrice);
        items.set('isOrdered', false);

        items.save(null, {
            success: function (items) {
                // Execute any logic that should take place after the object is saved.
                $('#selectSizeModal').modal('toggle')

                $scope.sizeS = 0;
                $scope.sizeM = 0;
                $scope.sizeL = 0;
                $scope.sizeXL = 0;
                $scope.sizeXXL = 0;

                //alert('New object created with objectId: ' + items.id);
            },
            error: function (items, error) {
                // Execute any logic that should take place if the save fails.
                // error is a Parse.Error with an error code and message.
                alert('There is an error add item to cart: ' + error.message);
            }
        });


    }
});