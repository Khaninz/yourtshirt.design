﻿angular.module('yourTshirtDesign')
.controller('ManageOrderController', function ($scope, $http) {

    
    var currentUser = Parse.User.current();
    

    if (currentUser) {
        //query for user's orders
        var Orders = Parse.Object.extend("Orders");
        var query = new Parse.Query(Orders);
        query.equalTo('createdBy', currentUser);
        
        query.include(['itemsInOrder.selectedShirtDesign']);
        query.descending('createdAt');
        query.find({
            success: function (results) {
                //alert('there is ' + results.length + ' user orders')
                console.log(results)

                $scope.userOrders = results;

                $scope.$apply();

            }, error: function (error) {
                alert(error.message);
            }
        });


    } else {
        //tell user to login first.
    }

    //GET METHODS
    $scope.getOrderTime = function (order) {
        return order.createdAt; //or use order.updatedAt
    }
    $scope.getOrderPrice = function (order) {
        return order.get('checkOutPrice');
    }
    $scope.getOrderPaidStatus = function (order) {
        var isPaid = order.get('status_paid');
        //console.log('isPaid' + isPaid);
        if (isPaid) {
            return 'Paid';
        } else {
            return 'Not paid';
        }
    }
    $scope.getOrderDeliveryStatus = function (order) {
        var isDelivered = order.get('status_deliver');
        //console.log(isDelivered);
        if (isDelivered) {
            return 'Delivered';
        } else {
            return 'Not yet delivered';
        }
    }

    $scope.getItemsInOrder = function (order) {
        var itemDetails = order.get('itemsInOrder');
        console.log(itemDetails);
        return itemDetails;
    }
    $scope.getShirtDesign = function (item) {

    }

    $scope.getItemTypeBackground = function (item) {
        var shirtDesign = item.get('selectedShirtDesign');
        var itemType = shirtDesign.get('item');
        console.log(itemType);
        var color = shirtDesign.get('color');
        console.log(color);
        var backgroundURL = 'url(../img/items/' + itemType + '/front-' + color + '.png)'
        console.log(backgroundURL);
        return backgroundURL;
    }

    $scope.getDesignImageURL = function (item) {
        var shirtDesign = item.get('selectedShirtDesign');
        var image = shirtDesign.get('thumbnail')
        var imageURL = image.url();
        return imageURL;
    }
    
});