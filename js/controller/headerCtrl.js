﻿angular.module('yourTshirtDesign')
.controller('headerCtrl', function ($scope) {

    $scope.signUpButtonText = 'Sign up!';
    
    //function to add data to view from parse user when logged in, signed up, and check current user
    var updateUserInfoToView = function (parseUser) {
        console.log('update to view!')
        console.log(parseUser.id);
        $scope.FirstName = parseUser.get('firstName');
        $scope.LastName = parseUser.get('lastName');
        $scope.Id = parseUser.id;
    }


    //Just for testing
    $scope.helloHeader = function () {
        console.log("Hi This is function in header control!")
    }

    //Check if there is any current user
    var currentUser = Parse.User.current();
    if (currentUser) {
        console.log();
        console.log('There is current user!')
     
        $scope.logInState = true;
        updateUserInfoToView(currentUser);        

    } else {
        
        console.log('There is no current user.')

    }    

    //Sign in
    $scope.signIn = function () {
        
        var email = $scope.userEmail;
        var password = $scope.userPassword;

        Parse.User.logIn(email, password, {

            success: function (user) {               
                console.log('login success');
                console.log(user);
                $scope.$apply(function () {
                    $scope.logInState = true;
                    updateUserInfoToView(user);
                });
                
            },
            error: function (user, error) {
                // The login failed. Check error to see why.
                console.log('login failed '+ error.message);
            }
        });
        
    }

    //Sign up
    $scope.signUp = function () {

        //$scope.signUpButtonText = 'Signing up...';
        //$scope.$apply();

        console.log('signing up...')

        //check confirm password
        if ($scope.signUpPassword == $scope.signUpConfirmPassword) {
            var user = new Parse.User();
            user.set("username", $scope.signUpEmail);
            user.set("password", $scope.signUpPassword);
            user.set("email", $scope.signUpEmail);
            user.set("firstName", $scope.signUpFirstName);
            user.set("lastName", $scope.signUpLastName);

            

            user.signUp(null, {
                success: function (user) {
                    // Hooray! Let them use the app now.
                    console.log('sign up success!');
                    $scope.$apply(function () {
                        $scope.logInState = true;
                        updateUserInfoToView(user);
                        $scope.signingUp = false;
                        signUpSuccess();
                    });

                    //close modal when success
                    $('#myModal').modal('hide')
                    
                },
                error: function (user, error) {

                    // Show the error message somewhere and let the user try again.
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        } else {
            alert('password does not match, please try again.')
        }

        //$scope.signUpButtonText = 'Sign up!';
        //$scope.$apply();
    }

    //logout
    $scope.logOut = function () {
        //userLoginService.userLogOut(updateLoginState);
        Parse.User.logOut();
        $scope.logInState = false;
        location.reload();
        
    }

    //upon sign up success clear all field
    var signUpSuccess = function () {

        $scope.signUpFirstName = "";
        $scope.signUpLastName = "";
        $scope.signUpEmail = "";
        $scope.signUpPassword = "";
        $scope.signUpConfirmPassword = "";
    }

    //clear field when signin success
    var signInSuccess = function () {
       
        $scope.userEmail = "";
    }

    
    $scope.fbLogIn = function () {

        Parse.FacebookUtils.logIn("public_profile,email", {
            success: function (user) {
                if (!user.existed()) {
                    alert("User signed up and logged in through Facebook!");
                } else {
                    alert("User logged in through Facebook!");
                }

                FB.api(
                    '/me',
                    {
                        fields: 'last_name,first_name,email'
                        
                    },
                    function (response) {
                        if (response && !response.error) {
                            /* handle the result */

                            user.set('username', response.email);
                            user.set('email', response.email);
                            user.set('firstName', response.first_name);
                            user.set('lastName', response.last_name);
                            user.save(null, {
                                success: function (user) {
                                    console.log('save user success!');
                                            $scope.$apply(function () {
                                                $scope.logInState = true;
                                                updateUserInfoToView(user);
                                            });
                                }, error: function (user,error) {
                                    //if error logging the user out.
                                    console.log('There is error update user with fb information');// example: duplicate fb email with existing from signing up with email directly.
                                    user.set('useFbInfoFailed', true); //for later monitor
                                    user.save();
                                }
                            });

                            
                        }
                    });

            },
            error: function (user, error) {
                console.log(error.message);
                alert("User cancelled the Facebook login or did not fully authorize: " + error.message);
            }
        });

    }



})