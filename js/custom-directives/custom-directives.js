﻿angular.module('yourTshirtDesign')
    .directive('header', function () {
        return {
            templateUrl: 'templates/header.html',
            controller: 'headerCtrl'

        }

    });


angular.module('yourTshirtDesign')
    .directive('signUpModal', function () {
    return {
        templateUrl: '../templates/signUpModal.html',
        controller:'headerCtrl'
    };
    });

angular.module('yourTshirtDesign')
    .directive('qualityModal', function () {
        return {
            templateUrl: '../templates/qualityModal.html',
            
        };
    });

angular.module('yourTshirtDesign')
    .directive('deliveryModal', function () {
        return {
            templateUrl: '../templates/deliveryModal.html',

        };
    });

angular.module('yourTshirtDesign')
    .directive('pricingModal', function () {
        return {
            templateUrl: '../templates/pricingModal.html',

        };
    });

angular.module('yourTshirtDesign')
    .directive('selectSizeModal', function () {
        return {
            templateUrl:'../templates/selectSizeModal.html',
            //controller:'CanvasControls'
        }
    });

angular.module('yourTshirtDesign')
    .directive('addAddressModal', function () {
        return {
            templateUrl: '../templates/addAddressModal.html',
            //controller: 'paymentController'
        }
    });

angular.module('yourTshirtDesign')
    .directive('addPaymentModal', function () {
        return {
            templateUrl: '../templates/addPaymentModal.html',
            //controller: 'paymentController'
        }
    });

angular.module('yourTshirtDesign')
    .directive('reviewConfirmModal', function () {
        return {
            templateUrl: '../templates/reviewConfirmModal.html',
            //controller: 'paymentController'
        }
    });